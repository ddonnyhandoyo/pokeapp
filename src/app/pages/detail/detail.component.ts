import { BreakpointObserver, BreakpointState, Breakpoints } from '@angular/cdk/layout';
import { CommonModule } from '@angular/common';
import { ChangeDetectorRef, Component } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ActivatedRoute } from '@angular/router';
import { NavigationBarComponent } from 'src/app/components/navigation-bar/navigation-bar.component';
import { PokemonDetailComponent } from 'src/app/components/pokemon-detail/pokemon-detail.component';
import { PokeDataService } from 'src/app/services/pokemon/pokedata.service';

@Component({
    selector: 'detail-page',
    standalone: true,
    imports: [CommonModule, NavigationBarComponent, FlexLayoutModule, PokemonDetailComponent],
    templateUrl: './detail.component.html',
    styleUrl: './detail.component.scss',
})
export class DetailPageComponent {
    smallerScreen!: boolean;
    private query: string | null = null;

    constructor(
        private breakpointObserver: BreakpointObserver,
        private changeDetectorRef: ChangeDetectorRef,
        private activatedRoute: ActivatedRoute,
        private pokeDataService: PokeDataService
    ) {}

    ngOnInit(): void {
        this.breakpointObserver
            .observe([
                Breakpoints.TabletPortrait,
                Breakpoints.TabletLandscape,
                Breakpoints.HandsetPortrait,
                Breakpoints.HandsetLandscape,
                Breakpoints.WebPortrait,
            ])
            .subscribe({
                next: (result: BreakpointState): void => {
                    this.smallerScreen = result.matches;
                    this.changeDetectorRef.detectChanges();
                },
            });
        this.query = this.activatedRoute.snapshot.queryParamMap.get('query');
        if (this.query) {
            this.pokeDataService.getPokemonDataByName(this.query);
        }
    }
}
