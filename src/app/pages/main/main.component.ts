import { BreakpointObserver, BreakpointState, Breakpoints } from '@angular/cdk/layout';
import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatPaginatorModule, PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { NamedAPIResource, NamedAPIResourceList } from 'pokenode-ts';
import { filter } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import { NavigationBarComponent } from 'src/app/components/navigation-bar/navigation-bar.component';
import { PokemonDetailComponent } from 'src/app/components/pokemon-detail/pokemon-detail.component';
import { PokeDataService } from 'src/app/services/pokemon/pokedata.service';
@Component({
    selector: 'main-page',
    standalone: true,
    imports: [CommonModule, NavigationBarComponent, FlexLayoutModule, PokemonDetailComponent, MatPaginatorModule],
    templateUrl: './main.component.html',
    styleUrls: ['./main.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MainPageComponent {
    small_screen!: boolean;
    page = 1;
    path_image = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/';

    constructor(
        private breakpointObserver: BreakpointObserver,
        private changeDetectorRef: ChangeDetectorRef,
        private pokeDataService: PokeDataService,
        private router: Router
    ) {}

    get paging_data$(): Observable<NamedAPIResourceList | undefined> {
        return this.pokeDataService.paging_data$.pipe(
            filter((pd: NamedAPIResourceList | undefined): boolean => pd !== undefined)
        );
    }

    ngOnInit(): void {
        this.breakpointObserver
            .observe([
                Breakpoints.TabletPortrait,
                Breakpoints.TabletLandscape,
                Breakpoints.HandsetPortrait,
                Breakpoints.HandsetLandscape,
                Breakpoints.WebPortrait,
            ])
            .subscribe({
                next: (result: BreakpointState): void => {
                    this.small_screen = result.matches;
                    this.changeDetectorRef.detectChanges();
                },
            });
        this.page = 1;
        this.pokeDataService.getPokemonDataList(this.page);
        // this.pokeDataService.getPokemonDataUsingType(this.page);
    }

    handlePageEvent(e: PageEvent) {
        this.page = e.pageIndex + 1;
        this.pokeDataService.getPokemonDataList(this.page);
    }

    trackByIndex = (idx: number): number => {
        return idx;
    };

    getImage(pokemon_dt: NamedAPIResource) {
        const uri_index = pokemon_dt.url.replace('https://pokeapi.co/api/v2/pokemon/', '');
        return this.path_image + uri_index.replace('/', '') + '.svg';
    }
    goToDetail(pokemon_dt: NamedAPIResource) {
        this.router.navigate(['/detail'], { queryParams: { query: pokemon_dt?.name } });
    }
}
