import { Injectable } from '@angular/core';
import { BreakpointObserverService } from '../breakpoint-observer/breakpoint-observer.service';

@Injectable({
    providedIn: 'root',
})
export class InitService {
    constructor(private breakpointObserverService: BreakpointObserverService) {}

    initializeApp(): Promise<void> {
        return new Promise<void>(async (resolve: any, reject: any): Promise<void> => {
            this.breakpointObserverService.init();
            resolve();
        });
    }
}
