import { Injectable } from '@angular/core';
import { NamedAPIResourceList, Pokemon, PokemonClient, Type } from 'pokenode-ts';
import { BehaviorSubject, Observable } from 'rxjs';
import { MessageService } from '../message/message.service';

@Injectable({
    providedIn: 'root',
})
export class PokeDataService {
    private readonly pokeAPI: PokemonClient = new PokemonClient();
    private readonly pokemon: BehaviorSubject<Pokemon | undefined> = new BehaviorSubject<Pokemon | undefined>(
        undefined
    );
    readonly pokemon$: Observable<Pokemon | undefined> = this.pokemon;
    private readonly paging_data: BehaviorSubject<NamedAPIResourceList | undefined> = new BehaviorSubject<
        NamedAPIResourceList | undefined
    >(undefined);
    readonly paging_data$: Observable<NamedAPIResourceList | undefined> = this.paging_data;

    private readonly list_type: BehaviorSubject<NamedAPIResourceList | undefined> = new BehaviorSubject<
        NamedAPIResourceList | undefined
    >(undefined);
    readonly list_type$: Observable<NamedAPIResourceList | undefined> = this.list_type;

    private readonly list_type_with_other_data: BehaviorSubject<Type | undefined> = new BehaviorSubject<
        Type | undefined
    >(undefined);
    readonly list_type_with_other_data$: Observable<Type | undefined> = this.list_type_with_other_data;

    get currentPokemon(): Pokemon | undefined {
        return this.pokemon.getValue();
    }

    constructor(private messageService: MessageService) {}

    async getPokemonDataByName(name: string): Promise<void> {
        if (name) {
            return this.pokeAPI
                .getPokemonByName(name.toLowerCase())
                .then((pokemon: Pokemon): void => this.pokemon.next(pokemon))
                .catch((): void => this.messageService.showMessage('Pokemon Data Not Found'));
        }
    }

    async getPokemonDataList(current_page: number, limit: number = 10): Promise<void> {
        const offset = (current_page - 1) * limit;
        return this.pokeAPI
            .listPokemons(offset, limit)
            .then((rl: NamedAPIResourceList): void => this.paging_data.next(rl))
            .catch((): void => this.messageService.showMessage('Pokemon Data List Not Found'));
    }

    async getListType(): Promise<void> {
        return this.pokeAPI
            .listTypes()
            .then((rl: NamedAPIResourceList): void => this.list_type.next(rl))
            .catch((): void => this.messageService.showMessage('Pokemon Data List Not Found'));
    }

    async getPokemonDataUsingType(current_page: number, limit: number = 10): Promise<void> {
        const offset = (current_page - 1) * limit;
        return this.pokeAPI
            .getTypeByName('rock')
            .then((rl: Type): void => this.list_type_with_other_data.next(rl))
            .catch((): void => this.messageService.showMessage('Pokemon Data List Not Found'));
    }

    resetPokemonDetailData() {
        this.pokemon.next(undefined);
    }
}
