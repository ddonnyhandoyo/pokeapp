import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class BreakpointObserverService {
    private readonly small_bp: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    small_bp$: Observable<boolean> = this.small_bp;

    constructor(private breakpointObserver: BreakpointObserver) {}
    init(): void {
        this.breakpointObserver
            .observe([
                Breakpoints.TabletPortrait,
                Breakpoints.TabletLandscape,
                Breakpoints.HandsetPortrait,
                Breakpoints.HandsetLandscape,
                Breakpoints.WebPortrait,
            ])
            .subscribe({
                next: (result: BreakpointState): void => {
                    this.small_bp.next(result.matches);
                },
            });
    }
}
