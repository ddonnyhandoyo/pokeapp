import { APP_INITIALIZER, ApplicationConfig, importProvidersFrom } from '@angular/core';
import { provideRouter } from '@angular/router';

import { provideHttpClient, withJsonpSupport } from '@angular/common/http';
import { MatChipsModule } from '@angular/material/chips';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { routes } from './app.routes';
import { InitService } from './services/init/init.service';
function initializeAppFactory(initService: InitService) {
    return (): void => {
        initService.initializeApp().then();
    };
}

export const appConfig: ApplicationConfig = {
    providers: [
        provideRouter(routes),
        provideHttpClient(withJsonpSupport()),
        importProvidersFrom(BrowserModule),
        importProvidersFrom(BrowserAnimationsModule),
        importProvidersFrom(MatSnackBarModule),
        importProvidersFrom(MatChipsModule),
        InitService,
        {
            provide: APP_INITIALIZER,
            useFactory: initializeAppFactory,
            deps: [InitService],
            multi: true,
        },
    ],
};
