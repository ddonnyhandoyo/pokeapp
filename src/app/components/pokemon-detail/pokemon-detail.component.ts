import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { Pokemon } from 'pokenode-ts';
import { Observable, filter } from 'rxjs';
import { BreakpointObserverService } from '../../services/breakpoint-observer/breakpoint-observer.service';
import { PokeDataService } from '../../services/pokemon/pokedata.service';
import { pokemon_type_colors } from '../../shared/pokemon-type-colors.const';

@Component({
    selector: 'app-pokemon-detail',
    standalone: true,
    imports: [CommonModule, FlexLayoutModule, MatChipsModule, MatCardModule],
    templateUrl: './pokemon-detail.component.html',
    styleUrls: ['./pokemon-detail.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PokemonDetailComponent {
    pokemon_type_color = pokemon_type_colors;

    get small_bp$(): Observable<boolean> {
        return this.breakpointObserverService.small_bp$;
    }

    get pokemon$(): Observable<Pokemon | undefined> {
        return this.PokeDataService.pokemon$.pipe(
            filter((pokemon: Pokemon | undefined): boolean => pokemon !== undefined)
        );
    }

    constructor(
        private PokeDataService: PokeDataService,
        private breakpointObserverService: BreakpointObserverService
    ) {}
}
