import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { Router } from '@angular/router';
import { NamedAPIResourceList } from 'pokenode-ts';
import { filter } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import { BreakpointObserverService } from 'src/app/services/breakpoint-observer/breakpoint-observer.service';
import { pokemon_type_colors } from 'src/app/shared/pokemon-type-colors.const';
import { PokeDataService } from '../../services/pokemon/pokedata.service';

@Component({
    selector: 'app-navigation-bar',
    standalone: true,
    imports: [CommonModule, MatToolbarModule, FlexLayoutModule, MatIconModule, MatButtonModule, MatChipsModule],
    templateUrl: './navigation-bar.component.html',
    styleUrls: ['./navigation-bar.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavigationBarComponent implements OnInit {
    pokemon_type_color = pokemon_type_colors;
    @Input() show_home_button = false; // show home back button
    @ViewChild('searchInput') search_input_ref: ElementRef<HTMLInputElement> | undefined;

    constructor(
        private pokeDataService: PokeDataService,
        private router: Router,
        private breakpointObserverService: BreakpointObserverService
    ) {}

    get small_bp$(): Observable<boolean> {
        return this.breakpointObserverService.small_bp$;
    }

    get types$(): Observable<NamedAPIResourceList | undefined> {
        return this.pokeDataService.list_type$.pipe(
            filter((dt: NamedAPIResourceList | undefined): boolean => dt !== undefined)
        );
    }

    ngOnInit(): void {
        this.pokeDataService.getListType();
    }

    searchPokemon(): void {
        if (this.search_input_ref === undefined) {
            return;
        }
        const value = this.search_input_ref.nativeElement.value;
        if (value) {
            if (this.search_input_ref !== undefined) this.search_input_ref.nativeElement.value = '';
            this.router.navigate(['/detail'], { queryParams: { query: value } });
        }
    }

    backToMainPage() {
        // reset pokemon data
        this.pokeDataService.resetPokemonDetailData();
        this.router.navigate(['/main'], { replaceUrl: true });
    }

    // getBackgroundColorByType(res: NamedAPIResource): string {
    //     const color = this.pokemon_type_color?.[res?.name] ? this.pokemon_type_color[res?.name] : '#ddd';
    //     return color;
    // }
}
