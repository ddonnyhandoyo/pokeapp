import { Routes } from '@angular/router';

export const routes: Routes = [
    { path: '', redirectTo: 'main', pathMatch: 'full' },
    { path: 'main', loadComponent: () => import('./pages/main/main.component').then((mdl) => mdl.MainPageComponent) },
    {
        path: 'detail',
        loadComponent: () => import('./pages/detail/detail.component').then((mdl) => mdl.DetailPageComponent),
    },
    { path: '**', redirectTo: 'main', pathMatch: 'full' },
];
